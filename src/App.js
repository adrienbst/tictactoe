import React from "react";
import "./styles.scss";
import TicTacToe from './Components/TicTacToe/TicTacToe';
import Home from './Components/Home/Home'
import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom";

export default function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/multi-local" element={<TicTacToe />} />
                <Route path="/singleplayer" element={<TicTacToe />} />
            </Routes>
        </BrowserRouter>
    );
}

import React, { useState, useEffect } from "react";
import "./Historic.css";

//Fonction Scoreboard fonctionne presque comme une classe
const Historic = (props) => {

    //useState est un hook d'état, il fonctionne comme des setters.
    const [historic, setHistoric] = useState([]);
    const [startPlayer, setStartPlayer] = useState();

    //Get the change of value "Winner in component TicTacToe"
    useEffect(()=>{
        //La variable squares prend en valeur la copie de cells
        let plays = [...historic];

        //Incrémente l'historique
        plays[props.turn-1] = props.play;
        for(let i = props.turn; i < 9; i++){
            plays[i] = "";
        }

        //On détermine 1er jouer à avoir joué
        //C'est utile pour savoir qui a joué quel coup
        if(props.turn === 1){
            for(let i = 0; i < 9; i++){
                if(plays[0][i]){
                    setStartPlayer(plays[0][i])
                }
            }
        }

        setHistoric(plays);

    },[props.play])

    //Fonction pour gérer les cellules du tableau
    const Cell = ({ num }) => {
        return (
            <td className="historic" onClick={() => props.timeTravel(historic[num], num, startPlayer)}>
                {historic[num]}
            </td>
        );
    };

    //render
    return (
        <table>
            <thead>
            <tr>
                <th> Historic </th>
            </tr>
            </thead>
            <tbody>
            {historic.map((play, key) => {
                return (
                    <tr>
                        <Cell num={key} />
                    </tr>
                );
            })}
            </tbody>
        </table>
    );
};

export default Historic;

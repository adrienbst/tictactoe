import "./Navbar.css";
import {Link} from "react-router-dom";
import {MdExitToApp} from 'react-icons/md';
import React from "react";

const Navbar = () => {

    const pathname = window.location.pathname

    //render
    return (
        <div id="navbar">
            <div id="logo" className={"text-primary"}>TicTacToe</div>
            {pathname !== "/" && (
                <Link to="/" className={"btn btn-primary me-4"}>Quit <MdExitToApp id={"quit-icon"}/></Link>
            )}
        </div>
    );
};

export default Navbar;

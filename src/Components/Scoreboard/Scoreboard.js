import React, { useState, useEffect } from "react";
import "./Scoreboard.css";

//Fonction Scoreboard fonctionne presque comme une classe
const Scoreboard = (props) => {

    //useState est un hook d'état, il fonctionne comme des setters.
    const [score, setScore] = useState(Array(2).fill(""));

    //Get the change of value "Winner in component TicTacToe"
    useEffect(()=>{
        //La variable squares prend en valeur la copie de cells
        let squares = [...score];

         if (props.winner !== "Nobody" && props.winner) {
             props.winner === "x"? squares[0]++ : squares[1]++
         }

         setScore(squares);

    },[props.winner])

    //Fonction pour gérer les cellules du tableau
    const Cell = ({ num }) => {
        return (
            <td>
                {score[num]}
            </td>
        );
    };

    //render
    return (
        <div>
            <table id="scoreboard">
                <thead>
                <tr>
                    <th> x </th>
                    <th> o </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <Cell num={0} />
                    <Cell num={1} />
                </tr>
                </tbody>
            </table>
        </div>
    );
};

export default Scoreboard;

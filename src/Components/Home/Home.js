import "./Home.css";
import { Button } from 'react-bootstrap';
import {Link} from "react-router-dom";
import Navbar from "../Navbar/Navbar";
import React from "react";
import ScrollToTop from "../ScrollToTop/ScrollToTop";

const Home = () => {

    //render
    return (
        <div className={"w-100 d-flex flex-column align-items-center"}>
            <Navbar />
            <ScrollToTop />
            <div id="menu-container">
                <h1 className={"text-primary"}>TicTacToe</h1>

                <Link to="/singleplayer" className={"btn btn-primary"}>Singleplayer</Link>
                <Link to="/multi-local" className={"btn btn-primary"}>Local Multiplayer</Link>
                <Button as="input" type="reset" value="Multiplayer" disabled />
                <Button as="input" type="reset" value="Options" disabled />
                {/*<Link to="/" className={"btn btn-primary mt-2"}>Multiplayer</Link>*/}
                {/*<Link to="/" className={"btn btn-primary mt-2"}>Options</Link>*/}
            </div>
        </div>
    );
};

export default Home;

import React, {useEffect, useState} from "react";
import Scoreboard from '../Scoreboard/Scoreboard';
import Historic from '../Historic/Historic';
import "./TicTacToe.css";
import Navbar from "../Navbar/Navbar";
import { useAspectRatio } from 'use-aspect-ratio';
import "../../styles.scss";
import {forEach} from "react-bootstrap/ElementChildren";

//Fonction ticTacToe fonctionne presque comme une classe
const TicTacToe = () => {
    //Variable turn et cells
    //Fonctionnent comme les attributs privates d'une classe
    //useState est un hook d'état, il fonctionne comme des setters.
    const [turn, setTurn] = useState("x");
    const [cells, setCells] = useState([]);
    const [winner, setWinner] = useState();
    const [nbTurn, setNbTurn] = useState(0);
    const pathname = window.location.pathname;

    //Fonction pour déclarer un vainqueur
    const checkForWinner = (squares, test) => {
        //définir les conditions de victoire
        let combos = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        //Ici on va regarder pour chaque combo:
        //Si on a trois fois le même signe à l'emplacement d'un combo.
        //On déclare un vainqueur en regardant quel signe est à l'origine du combo gagnant.

        let win = 0;
        combos.forEach((combo) => {

            //On vérifie que toutes les cases ne soient pas nulles pour éviter
            //qu'une série de cases vides soient à l'origine d'un combo.
            if (
                squares[combo[0]] &&
                squares[combo[0]] === squares[combo[1]] &&
                squares[combo[1]] === squares[combo[2]]
            ) {
                win = squares[combo[0]];
            }
        });

        //On vérifie qu'il n'y a pas match nul avant de continuer
        if(win && !test){
            setWinner(win)
        } else if (nbTurn === 8 && !test){
            setWinner("Nobody")
        }
        return win;
    };

    //use hooks to set attributes to null
    const handleRestart = () => {
        setWinner("");
        setCells(Array(9).fill(""));
        setNbTurn(0);
    };

    //Fonction pour gérer les clics
    const handleClick = (num) => {
        //If a winner has been declared, say it and stop
        if (winner) {
            alert("Game is end, please restart");
            return;
        }
        //If cell has been already clicked, say it and stop
        if (cells[num]) {
            return;
        }
        setNbTurn(nbTurn + 1);

        //La variable squares prend en valeur la copie de cells
        let squares = [...cells];

        if (turn === "x") {
            squares[num] = "x";
            setTurn("o");
        } else {
            squares[num] = "o";
            setTurn("x");
        }

        checkForWinner(squares);
        setCells(squares);
    };

    //Computer move
    useEffect(()=>{

        if (pathname === '/singleplayer' && turn === 'o' && !winner){
            let squares = [...cells];
            let difficulty = 2;

            switch(difficulty){
                case 1:
                    randomPlay(squares);

                case 2:
                    artificialIntelligence(squares);

            }
            checkForWinner(squares);
            setCells(squares);
        }
    },[turn, winner])

    const artificialIntelligence = (squares) =>{

        //On simule les sénarios possibles

        let play = false;
        //On essai de jouer sur toutes les cases
        for(let i = 0; i < 9; i++) {
            //On simule des coups sur les cases vides pour voir si on peut gagner
            if (!squares[i]){
                let predict = [...squares];
                predict[i] = "o";
                //Si on peut gagner on joue ce coup
                if (checkForWinner(predict, 1)) play = i
            }
        }

        play? squares[play] = 'o' : squares = randomPlay(squares)

        return squares;
    }

    const randomPlay = (squares) => {
        let rand = Math.floor(Math.random() * 9);
        while (squares[rand]){
            rand = Math.floor(Math.random() * 9);
        }
        setNbTurn(nbTurn + 1);

        squares[rand] = "o";
        setTurn("x");

        return squares
    }

    const timeTravel = (play, turn, start) => {

        if(!play || winner){
            return
        }

        //Détermine qui a joué quel tour
        if(turn%2 === 0){
            start === "x"? setTurn("o") : setTurn("x")
        } else {
            setTurn(start)
        }
        setNbTurn(turn+1)
        setCells(play)
    }

    //Fonction pour gérer les cellules du tableau
    const Cell = ({ num }) => {
        return (
            <td
                onClick={() => {
                    handleClick(num);
                }}
            >
                {cells[num]}
            </td>
        );
    };

    const ref = useAspectRatio(1);

    //render
    return (
        <div className="game-container">
            <Navbar />
            <div id="game" className={"text-primary"}>
                <div id="scoreFrame">Turn: {turn}
                    <Scoreboard winner={winner}/>
                </div>
                <div id="gameFrame">
                    <table id="tictactoe" ref={ref}>
                        <tbody>
                        <tr>
                            <Cell num={0} />
                            <Cell num={1} />
                            <Cell num={2} />
                        </tr>
                        <tr>
                            <Cell num={3} />
                            <Cell num={4} />
                            <Cell num={5} />
                        </tr>
                        <tr>
                            <Cell num={6} />
                            <Cell num={7} />
                            <Cell num={8} />
                        </tr>
                        </tbody>
                    </table>

                    {winner && (
                        <div id="overlay">
                        </div>
                    )}

                    {winner && (
                        <div id="text">
                            <p>{winner} is the winner!</p>
                            <button className="button" onClick={() => handleRestart()}>Play Again</button>
                        </div>
                    )}
                </div>

                <div id="historicFrame">
                    <Historic play={cells} turn={nbTurn} player={turn} timeTravel={timeTravel}/>
                </div>
            </div>
        </div>
    );
};

export default TicTacToe;
